#!/bin/bash

project_dir=$(dirname $0)
build_dir=$project_dir/build

# Update deps
cmake -DFETCHCONTENT_UPDATES_DISCONNECTED=OFF -B $build_dir

# Restore FETCHCONTENT_UPDATES_DISCONNECTED
cmake -DFETCHCONTENT_UPDATES_DISCONNECTED=ON -B $build_dir
