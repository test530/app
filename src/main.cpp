#include <stdlib.h>
#include <string_view>

#include <liba/liba.h>
#include <libb/libb.h>

using namespace std::string_view_literals;

int main()
{
    auto name = "Иван"sv;
    liba::sayHello(name);
    libb::sayGoodbye(name);
    return EXIT_SUCCESS;
}
